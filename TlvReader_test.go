package main

import (
	"testing"
)

// var casosPrueba [8]string
// casosPrueba[0] = ""
// casosPrueba[1] = "55a"
// casosPrueba[2] = "A050255"
// casosPrueba[3] = "A050355"
// casosPrueba[4] = "N0511AB398765UJ1N23020A"
// casosPrueba[5] = "A0511AB398765UJ1N230200" OK
// casosPrueba[6] = "11AB398765UJ1A050200N23"
// casosPrueba[7] = "11A05AB398765UJ102N2300"

func TestReadCadenaVacia(t *testing.T) {
	cadena := ""
	val, err := read(cadena)
	_ = val

	if err != nil {
		t.Error("La cadena esta vacia")
	}
}

func TestReadCadenaFormatoIncorrecto(t *testing.T) {
	cadena := "55a"
	val, err := read(cadena)
	_ = val

	if err != nil {
		t.Error("La cadena no tiene el formato esperado")
	}
}

func TestReadCadenaLargoMenorEspecificado(t *testing.T) {
	cadena := "A050355"
	val, err := read(cadena)
	_ = val

	if err != nil {
		t.Error("El campo valor no tiene el largo especificado")
	}
}
func TestReadCadenaTipoDatoNoNumerico(t *testing.T) {
	cadena := "N0511AB398765UJ1N23020A"
	val, err := read(cadena)
	_ = val

	if err != nil {
		t.Error("El tipo de dato no es numerico")
	}
}
func TestReadCadenaFormatoIncorrectoT(t *testing.T) {
	cadena := "11AB398765UJ1A050200N23"
	val, err := read(cadena)
	_ = val

	if err != nil {
		t.Error("La cadena no tiene el formato correcto")
	}
}
