# Test FB

Proyecto de prueba para la postulacion a la empresa fb

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

GO Lang

```
$ brew install go
```

## Running the tests

```
$ go test -v
```

## Deployment

```
$ go build .
$ ././falabella-test
```

![Saludos](https://i.ytimg.com/vi/KD3UmoWsGcM/hqdefault.jpg)