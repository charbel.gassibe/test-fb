package main

import (
	"testing"
)

func TestEsNumerico(t *testing.T) {
	cadena := "55a"
	val := esNumerico(cadena)

	if val == true {
		t.Error("La cadena no tiene el formato esperado")
	}
}
