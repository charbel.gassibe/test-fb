package main

import (
	"errors"
	"strconv"
)

const (
	tipoValorAlfaNumerico = "A"
	tipoValorNumerico     = "N"
	largoKeyTipo          = 3
	largoKeyLargo         = 2
)

func read(tlv string) (map[string]string, error) {

	mapTLV := make(map[string]string)

	if tlv == "" {
		return mapTLV, errors.New("La cadena esta vacia")
	}

	var largoString = len(tlv)
	var index = 0

	runes := []rune(tlv)
	for index < largoString {

		if (largoString - index) < (largoKeyTipo + largoKeyLargo) {
			return mapTLV, errors.New("no hay suficientes caracteres para leer")
		}

		var keyTipo = string(runes[index:(index + largoKeyTipo)])

		var tipoArray = []rune(keyTipo)
		var tipo = string(tipoArray[0:1])
		var tipoKey = string(tipoArray[1:3])

		if tipo != tipoValorAlfaNumerico && tipo != tipoValorNumerico {
			return mapTLV, errors.New("Tipo de dato no soportado")
		}

		index += largoKeyTipo
		var largoKey = string(runes[index:(index + largoKeyLargo)])
		index += largoKeyLargo

		largoNumerico, err := strconv.Atoi(largoKey)

		if (index + largoNumerico) > largoString {
			return mapTLV, errors.New("no hay suficientes caracteres para leer")
		}

		if err == nil {
			var valor = string(runes[index : index+largoNumerico])
			if tipo == tipoValorNumerico && !esNumerico(valor) {
				return mapTLV, errors.New("El valor " + valor + " no es numerico")
			}
			mapTLV[tipoKey] = valor
		}
		index += largoNumerico
	}
	return mapTLV, nil
}
